package kultprosvet.com.weatherforecastandroid.api;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


//Translation date into a readable format
public class DateConverter {
    public static final String DATE_FORMAT="d MMM yyyy";
    public static final int SEC_TO_MILLIS_COEFFICIENT=1000;
    public static final String LOCATE_LANGUAGE="us";
    public static final String LOCATE_REGION="UA";

    public static String convertDate(long dateInSeconds){
        long dateInMillis = dateInSeconds*SEC_TO_MILLIS_COEFFICIENT;
        Date date=new Date(dateInMillis);
        Locale ukrLocale = new Locale(LOCATE_LANGUAGE,LOCATE_REGION);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat(DATE_FORMAT,ukrLocale);
        String result = simpleDateFormat.format(date);
        return result;
    }
}
