package kultprosvet.com.weatherforecastandroid.api;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by User on 06.12.2016.
 */
public interface OpenWeatherApi {
    @GET("weather")
    //host prescribe once every
    Call<TodayForecast> getTodayForecast(@Query("q") String q, @Query("units") String units, @Query("APPID") String appid);

    //where  q - query ;units-notation;APP_ID- access key

    @GET("forecast/daily")
    Call<Forecast16> getForecast16(@Query("q") String q, @Query("units") String units, @Query("APPID") String appid);


}
