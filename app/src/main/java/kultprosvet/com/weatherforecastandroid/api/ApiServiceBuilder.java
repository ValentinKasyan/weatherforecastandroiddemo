package kultprosvet.com.weatherforecastandroid.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 09.01.2017.
 */
public class ApiServiceBuilder {
    private static final Retrofit.Builder RETROFIT = new Retrofit.Builder()
            .baseUrl(Config.API_URL)
            //converter makes the conversion of the Gson object in java objects
            .addConverterFactory(GsonConverterFactory.create());

    public static OpenWeatherApi getApiService() {
        return RETROFIT.build().create(OpenWeatherApi.class);
    }
}
