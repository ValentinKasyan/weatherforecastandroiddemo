package kultprosvet.com.weatherforecastandroid.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import kultprosvet.com.weatherforecastandroid.R;
import kultprosvet.com.weatherforecastandroid.api.ForecastItem;
import kultprosvet.com.weatherforecastandroid.databinding.RowForecastBinding;

/**
 * Created by User on 25.12.2016.
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.Holder> {
    private List<ForecastItem> items;

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_forecast,parent,false);
        return new ForecastAdapter.Holder(v);

    }




    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setItem(items.get(position));

    }

    public ForecastAdapter setItems(List<ForecastItem> items) {
        this.items = items;
        return this;
    }

    @Override
    public int getItemCount() {
        if (items==null) return 0;
        else return items.size();
    }



    public static class Holder extends RecyclerView.ViewHolder{
        private Context context;

        private RowForecastBinding binding;
        private ForecastItem item;
        public ForecastItem getItem() {
            return item;
        }


        public Holder setItem(ForecastItem item) {
            this.item = item;
            binding.setItem(item);
            getRowIcon(item.getWeather().get(0).getMain());
            return this;
        }


        public Holder(View itemView){
            super(itemView);
            context=itemView.getContext();
            binding=DataBindingUtil.bind(itemView);

        }
        private void getRowIcon(String weatherMainStatus){
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int size = Math.round(metrics.density * 100);
            int statusIcon=R.drawable.cloud;
            switch (weatherMainStatus){
                case "Thunderstorm" : statusIcon = R.drawable.tornado;
                    break;
                case "Rain" : statusIcon = R.drawable.rainy;
                    break;
                case "Clouds" : statusIcon = R.drawable.cloud;
                    break;
                case "Clear" : statusIcon = R.drawable.sunny;
                    break;
                case "Atmosphere" : statusIcon = R.drawable.cloud;
                    break;
                case "Snow" : statusIcon = R.drawable.snow;
                    break;
            }

                        Picasso.with(context).load(statusIcon)
                                .resize(size, size)
                                .centerInside()
                                .into(binding.rowIcon);
        }
    }
}
