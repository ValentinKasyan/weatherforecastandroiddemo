package kultprosvet.com.weatherforecastandroid.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kultprosvet.com.weatherforecastandroid.R;

public class CityDetailsactivity extends AppCompatActivity {

    private LocalBroadcastManager bm;
    BroadcastReceiver onHomeOpened = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //unsubscribe before the completion of the activity
            bm.unregisterReceiver(this);
            CityDetailsactivity.this.finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_detailsactivity);
        //register the message recipient from LocalBroadcastManager(template eventbus)
        bm=LocalBroadcastManager.getInstance(this);
        //register receiver
        bm.registerReceiver(onHomeOpened, new IntentFilter("home_screen_opened"));
    }
}
