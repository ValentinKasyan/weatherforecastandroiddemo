package kultprosvet.com.weatherforecastandroid.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import kultprosvet.com.weatherforecastandroid.R;
import kultprosvet.com.weatherforecastandroid.api.ApiServiceBuilder;
import kultprosvet.com.weatherforecastandroid.api.Config;
import kultprosvet.com.weatherforecastandroid.api.Forecast16;
import kultprosvet.com.weatherforecastandroid.api.OpenWeatherApi;
import kultprosvet.com.weatherforecastandroid.api.TodayForecast;
import kultprosvet.com.weatherforecastandroid.databinding.ActivityMainBinding;
import retrofit2.Call;
import retrofit2.Callback;

import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static  final String TAG= "logs";
    ActivityMainBinding binding;
    private OpenWeatherApi service;
    private ForecastAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        //interface OpenWeatherApi to work with  Retrofit
        service = ApiServiceBuilder.getApiService();
        //current weather forecast
        getTodayForecast();
        //week weather forecast
        getTodayForecast16();




    }


    public void getTodayForecast() {
        //asynchronous Retrofit method call
        service.getTodayForecast(Config.API_CITY,Config.API_ISO,Config.API_KEY).
                enqueue(new Callback<TodayForecast>() {
                    @Override
                    public void onResponse(Call<TodayForecast> call, Response<TodayForecast> response) {
                        binding.setForecast(response.body());
                        System.out.println(response.body().getWeather().get(0).getMain());
                        getImage();

                    }

                    @Override
                    public void onFailure(Call<TodayForecast> call, Throwable t) {
                       showAlertDialog(t);
                    }
                });

}
    public void getTodayForecast16(){
        //asynchronous Retrofit method call
        service.getForecast16(Config.API_CITY,Config.API_ISO,Config.API_KEY).
                enqueue(new Callback<Forecast16>() {
                    @Override
                    public void onResponse(Call<Forecast16> call, Response<Forecast16> response) {
                        adapter=new ForecastAdapter();
                        adapter.setItems(response.body().getForecastList());
                        binding.recycleview.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<Forecast16> call, Throwable t) {
                        showAlertDialog(t);

                    }
                });

    }

    public void getImage(){
        //find out display metric to scale images (recorded as 100dp x 100dp)
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        //find out real pixel density (how many pixels in the same dp)
        int size = Math.round(metrics.density*100);//(recorded as 100dp x 100dp)

       Picasso.with(this).load(R.drawable.cloud)
               .resize(size,size)
               .centerInside()
               .into(binding.imageView);
    }

    public void showAlertDialog(Throwable throwable){
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Error")
                .setMessage(throwable.getLocalizedMessage())
                .setPositiveButton("Close", null)
                .show();

    }

    @Override
    //create menu
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    //pressing on the menu processing
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG,"method onOptionsItemSelected run");
        switch (item.getItemId()){
            case R.id.add_city:
                Intent intent=new Intent(this,CityDetailsactivity.class);
                startActivity(intent);
                Log.d(TAG,"Pressing the menu button");
                break;
            case R.id.Exit:
                finish();
        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        //sending out notices on the application using the template eventbus

        LocalBroadcastManager bm = LocalBroadcastManager.getInstance(this);
        Intent intent=new Intent("home_screen_opened");
        bm.sendBroadcast(intent);
    }
}
